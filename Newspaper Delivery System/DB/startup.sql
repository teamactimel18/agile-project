DROP DATABASE IF EXISTS Project;
CREATE DATABASE IF NOT EXISTS Project;
USE Project;

SELECT 'CREATING EMPLOYEE TABLE' as 'INFO';

DROP TABLE IF EXISTS Product;
CREATE TABLE Product (
	product_ID INTEGER, NOT NULL, AUTO_INCREMENT,
	title VARCHAR(255) NOT NULL,
	cost NOT NULL, DOUBLE,
	date_of_issue NOT NULL, DATE,
	publisher VARCHAR(255) NOT NULL,,
	publication_frequency ENUM('Daily', 'Weekly','Bi-Weekly','Bi-Monthly','Monthly'),
	PRIMARY KEY(product_ID) );
	
SELECT 'INSERTING DATA INTO TABLE PRODUCT' as 'INFO';

INSERT INTO Product VALUES ( null, 'The Irish Times', 4.00, '06-02-2019','Irish Times', 'Daily',);
INSERT INTO Product VALUES ( null, 'Irish Daily Mail', 3.00, '06-02-2019','Daily Mail', 'Daily',);
INSERT INTO Product VALUES ( null, 'The Irish Sun', 3.50, '06-02-2019','The Sun', 'Daily',);
INSERT INTO Product VALUES ( null, 'The Sunday Times', 5.00, '06-02-2019','Sunday Times', 'Weekly',);
INSERT INTO Product VALUES ( null, 'Racing Post', 5.00, '06-02-2019','Racing Post', 'Daily',);

SELECT 'CHECKING TO SEE IF DATA INSERTED TO PRODUCT CORRECTLY' as 'INFO';

SELECT * FROM Product;

SELECT 'CREATING Geographical_Area TABLE' as 'INFO';

DROP TABLE IF EXISTS Geographical_Area;

CREATE TABLE Geographical_Area (
	zone_id INTEGER AUTO_INCREMENT ,
	name VARCHAR(255) NOT NULL,
	description VARCHAR(250) NOT NULL,
	PRIMARY KEY(zone_id) );
	
SELECT 'INSERTING DATA INTO TABLE Geographical_Area' as 'INFO';

INSERT INTO Geographical_Area VALUES ( null, 'Zone 1', 'Ballina Road',);
INSERT INTO Geographical_Area VALUES ( null, 'Zone 2', 'Coosan',);
INSERT INTO Geographical_Area VALUES ( null, 'Zone 3', 'Golden Island',);
INSERT INTO Geographical_Area VALUES ( null, 'Zone 4', 'Athlone Road',);
INSERT INTO Geographical_Area VALUES ( null, 'Zone 5', 'Town Center',);
INSERT INTO Geographical_Area VALUES ( null, 'Zone 6', 'Dublin Road',);


SELECT 'CHECKING TO SEE IF DATA INSERTED TO Geographical_Area CORRECTLY' as 'INFO';

SELECT * FROM Geographical_Area;

DROP TABLE IF EXISTS delivery;
CREATE TABLE delivery (
	delivery_id INTEGER AUTO_INCREMENT,
	delivery_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY(delivery_id)
);

INSERT INTO delivery(delivery_id) VALUES (null);

DROP TABLE IF EXISTS delivery_driver;
CREATE TABLE delivery_driver(
	driver_ID INTEGER,
	zone_id INTEGER,
	PRIMARY KEY(driver_id),
	FOREIGN KEY (zone_id) REFERENCES geographical_area (zone_id)
); 

INSERT INTO delivery_driver VALUES (1, 1);

CREATE TABLE customer ( 

PRIMARY KEY customer_id SMALLINT AUTO_INCREMENT , 
address_line1 VARCHAR(30) NOT NULL, 
address_line2 VARCHAR(30) NOT NULL, 
first_name VARCHAR(20) NOT NULL, 
last_name VARCHAR(20), 
zone_id SMALL INT(3), 
 ); 

INSERT INTO customers VALUES ( null, 'Dublin', 'Road', 'John', 'Doe', '001'); 
INSERT INTO customers VALUES ( null, '23', 'Monksland', 'Jane', 'Doe', '002'); 
INSERT INTO customers VALUES ( null, '93', 'Town Centre', 'Joe', 'Bloggs', '002'); 

CREATE TABLE invoice ( 
PRIMARY KEY invoice_id MEDIUM INT AUTO INCREMENT, 
FOREIGN KEY customer_id SMALLINT AUTO INCREMENT, 
total_cost VARCHAR(6) NOT NULL, 
); 

INSERT INTO invoice VALUES ( null, null, 123.95); 
INSERT INTO invoice VALUES ( null, null, 6.00); 

 

 

 
